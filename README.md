# Developer info for the MineClick project

## License information

The MineClick project is licensed under the [GNU GPLv3 licence](https://www.gnu.org/licenses/gpl-3.0.en.html) with [Commons Clause](https://commonsclause.com/).

**All copyrights belong to MineClick.net.** You're not allowed to:
- Redistribute the project's code.
- Use the project's code for commercial purposes.

## Local development

See the [README.md](https://gitlab.com/mineclick/docker#local-development) file in the Docker repo for instructions on
how to set up a local development environment.

## Contributing to the project

Anyone can contribute to the project by creating a merge request from a fork of the project. The merge request will be
reviewed by a project maintainer and merged if it meets the project's standards.

Code standards are not currently defined, but will be in the future, so please try to keep your code clean and readable,
otherwise your merge request may be rejected.

### Summary of MineClick's architecture

- [Messenger](https://gitlab.com/mineclick/messenger): A library that handles communication (using Redis) between
  servers and services. It's currently used to establish a connection between Ender, Bungee, Store and the Game servers.
  The library is bundled with the jar files of the servers and services that use it.
- [Ender](https://gitlab.com/mineclick/ender): A service that provides DB access through the Messenger library. It also
  provides a web interface for the status/settings site [ender.mineclick.net](https://ender.mineclick.net).
- [Bungee](https://gitlab.com/mineclick/bungee): A BungeeCord library that handles player connections and redirects them
  to the
  correct Game server.
- [Global](https://gitlab.com/mineclick/global): A Spigot library that contains common code that's mainly used by the
  Game
  repo.
- [Game](https://gitlab.com/mineclick/game): A Spigot library that handles the gameplay of the server. This is the main
  repo that you'll be working on if you want to add new features to the server.
- [Docker](https://gitlab.com/mineclick/docker): A repo that contains the Dockerfiles and docker-compose files used to
  deploy the server (Docker Swarm). It's the starting point for local development.
- [Config](https://gitlab.com/mineclick/config): A repo that contains the configuration files for the server such as
  island configurations, schematics, etc. It's used by the Game servers.
- [Website](https://gitlab.com/mineclick/website): A repo that contains the source code for the
  [mineclick.net](https://mineclick.net) website.

### Creating a new merge request

1. Fork the repo you want to contribute to. You can do this by clicking the "Fork" button in the top right corner of the
   repo's page.
2. Create a new branch from the `staging` branch.
3. Make your changes.
4. Commit your changes and push them to your fork.
5. Create a merge request from your fork to the original repo's `staging` branch.
6. Post a message in the `#dev-mr` channel on Discord with a link to your merge request.
7. If your merge request is accepted, it will be merged into the `staging` branch of the original repo.
8. If your merge request is rejected, you can make the necessary changes and push them to your fork. The merge request
   will be updated automatically.
9. If you have any questions, please write in the `#dev` channel on Discord.

### Creating a new issue

1. Go to the repo's "Issues" page.
2. Click the "New issue" button.
3. Fill in the issue's title and description.
4. Don't apply any labels to the issue. A project maintainer will do this for you.
5. Click the "Submit issue" button.
6. Mention the issue in Discord.